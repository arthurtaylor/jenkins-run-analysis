"""Main entrypoint for analyzing CI references to extract job timing info"""
import argparse
import logging
import sys

from analyzer.parser import analyze
from analyzer.descriptors.descriptor_factory import DescriptorFactory
from analyzer.exceptions import ChangeDetailNotFoundException, MultipleChangesFoundException

logging.basicConfig(level=logging.INFO, stream=sys.stdout)

parser = argparse.ArgumentParser(
                    prog='analyze.py',
                    description='Parses the CI log output and provides job timing information')

parser.add_argument('job_descriptor', help='the job to analyze. This can be a Gerrit Change ID' \
                ' (e.g. I3cc3048ef6ce4cb4634b8aafb653ca219a35bc89), a Gerrit Patch number' \
                ' (e.g. 1013266) or a URL to a Jenkins job (e.g. '\
                'https://integration.wikimedia.org/ci/job/' \
                'quibble-vendor-mysql-php74-noselenium-docker/164023/consoleFull' )

parser.add_argument('--repo', help='the target Gerrit repo. Defaults to ' \
                    '`mediawiki/extensions/Wikibase`. Only relevant when resolving patches ' \
                    'by patch number (e.g. 1013266)',
                        required = False, default = 'mediawiki/extensions/Wikibase' )

parser.add_argument('--format', help='output format for the analysis - `json` or `text`',
                        required = False, default = 'text' )

args = parser.parse_args()
try:
    analysis = analyze(DescriptorFactory.build_descriptor(args.job_descriptor, args.repo))
except ChangeDetailNotFoundException as e:
    print(f"Unable to resolve change with id {args.job_descriptor}")
    print(f"Does patch {e.get_patch_id()} belong to repo {e.get_repo()}?")
    print("Try setting the --repo argument")
    sys.exit(1)
except MultipleChangesFoundException as e:
    print(f"Unable to resolve change with id {args.job_descriptor}")
    print(f"Patch {e.get_change_id()} seems to have multiple changes associated")
    print("Try supplying the gerrit change number instead")
    sys.exit(1)

if analysis is not None:
    print(analysis.dump(format_name = args.format))
else:
    print("No run information found")
