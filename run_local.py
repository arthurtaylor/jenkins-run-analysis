"""Extract the ENV variables for a build be supplying the gerrit URL for the
   build parameters page"""

import sys
from typing import Dict

from argparse import ArgumentParser

from analyzer.descriptors.ci_job_descriptor import CiJobDescriptor
from analyzer.descriptors.yaml_job_descriptor import YamlJobDescriptor
from analyzer.parameter_manager import ParameterManager
from analyzer.config import Config
from analyzer.exceptions import InvalidConfigException

def get_arg_parser():
    """Return the argument parser for the command-line options"""
    parser = ArgumentParser()
    parser.add_argument(
        '--bash',
        help="Set to launch a bash terminal instead of running tests",
        action='store_true'
    )
    parser.add_argument(
        '--parallel-npm-install',
        help="Set to install npm dependencies for browser tests in parallel",
        action='store_true'
    )
    parser.add_argument(
        '--target',
        help="Target local folder to contain the Quibble run artefacts",
        default='quibble'
    )
    parser.add_argument(
        '--docker-image',
        help="Specify the docker image to use for this run"
    )
    parser.add_argument(
        '--quibble-dev',
        help="Specify the folder containing the quibble source code",
    )
    parser.add_argument(
        '--change',
        help="Specify the changeID to test with",
        default=None
    )
    parser.add_argument(
        '-e',
        '--env-override',
        help="Override an environment variable",
        default=[],
        action='append'
    )
    parser.add_argument('url')
    return parser

def get_env_overrides_dict(options) -> Dict:
    """Return the list of environment overrides as a dictionary"""
    res = {}
    for override in options.env_override:
        parts = override.split('=', 1)
        res[parts[0]] = parts[1]
    return res

def get_config():
    """Return the configuration options object"""
    options = get_arg_parser().parse_args()
    return Config(
        options.url,
        options.target,
        options.bash,
        options.docker_image,
        options.quibble_dev,
        options.change,
        options.parallel_npm_install,
        get_env_overrides_dict(options)
    )

if __name__ == '__main__':
    config = get_config()
    if config.source_is_yaml():
        file_path = config.get_file_path()
        if file_path is None:
            raise InvalidConfigException("Invalid YAML config")
        job = YamlJobDescriptor.from_yaml(file_path)
        env = job.get_env_variables()
    else:
        job = CiJobDescriptor.from_url(config.get_url())
        env = ParameterManager().parse_to_dictionary(job.get_parameters_url())
    if config.should_parallel_npm_install():
        env["QUIBBLE_SELENIUM_PARALLEL"] = "1"
    env.update(config.get_env_overrides())
    print(env)
    docker_commander = job.get_quibble_docker_command(config)
    if docker_commander is not None:
        docker_commander.launch(env)
    else:
        print("No docker command found in console log.")
        sys.exit(1)
