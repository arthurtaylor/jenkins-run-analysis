"""Extract the ENV variables for a build be supplying the gerrit URL for the
   build parameters page"""

import sys

from analyzer.parameter_manager import ParameterManager

if __name__ == '__main__':
    for line in ParameterManager().parse_to_env_file(sys.argv[1]):
        print(line)
