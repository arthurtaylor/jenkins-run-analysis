"""Tests of the log output parser class"""
from time import strptime

from unittest import TestCase

from analyzer.log_output_parser import extract_timestamp, extract_header, time_diff

class TestLogOutputParser(TestCase):
    """Tests of the LogOutputParser class"""

    def test_parse_timestamp(self):
        """Test that we can parse a timestamp"""
        log_string = '<span class="timestamp"><b>11:43:27</b> </span>' \
            'INFO:backend.ChromeWebDriver:Terminating ChromeWebDriver'
        self.assertEqual(
            strptime("11:43:27", "%H:%M:%S"),
            extract_timestamp(log_string),
            "Expected time to be parsed"
        )

    def test_parse_header(self):
        """Test that we can parse a header line"""
        header_string = '</div></div><div class="section" data-level="">' \
            '<div class="collapseHeader">PHPUnit extensions suite (with database)' \
            '<div class="collapseAction">' \
            '<p onClick="doToggle(this)">Hide Details</p></div></div><div class="expanded">' \
            '<span class="timestamp"><b>11:28:52</b> </span>' \
            'INFO:quibble.commands:&gt;&gt;&gt; Start: PHPUnit extensions suite (with database)'
        self.assertEqual(
            "PHPUnit extensions suite (with database)",
            extract_header(header_string),
            "expected header to be parsed"
        )

    def test_parse_header_unescape_entities(self):
        """Test that we unescape the html entities that we find"""
        header_string = '</div></div><div class="section" data-level="">' \
            '<div class="collapseHeader">PHPUnit extensions suite &lt;with database&gt;' \
            '<div class="collapseAction">' \
            '<p onClick="doToggle(this)">Hide Details</p></div></div><div class="expanded">' \
            '<span class="timestamp"><b>11:28:52</b> </span>' \
            'INFO:quibble.commands:&gt;&gt;&gt; Start: PHPUnit extensions suite (with database)'
        self.assertEqual(
            "PHPUnit extensions suite <with database>",
            extract_header(header_string),
            "expected header to be parsed"
        )

    def test_time_diff(self):
        """Test that we can diff two timestamps"""
        start = strptime("11:00:00", "%H:%M:%S")
        end = strptime("11:01:30", "%H:%M:%S")
        self.assertEqual(90, time_diff(start, end), "Expected time difference to be calculated")
