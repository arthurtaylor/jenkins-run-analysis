"""Tests for the analysis class"""
from unittest import TestCase
from unittest.mock import patch

from analyzer.analysis import Analysis
from analyzer.descriptors.descriptor_factory import DescriptorFactory

class TestAnalysis(TestCase):
    """Test cases for the Analysis class"""

    EXAMPLE_GERRIT_URL = "https://gerrit.wikimedia.org/r/c/mediawiki/extensions/Test/+/12345/"

    EXAMPLE_TEXT_DUMP = "Analysis for change mediawiki/extensions/Test~12345:\n" \
                        "\tJob Name (456): 123\n" \
                        "\t\tSection: 123\n"

    EXAMPLE_JSON_DUMP = '{"descriptor": {"url": '\
                        '"' + EXAMPLE_GERRIT_URL + '", '\
                        '"repo": "mediawiki/extensions/Test", "patch_number": "12345"}, "jobs": '\
                        '[{"job_id": 456, "job_name": "Job Name", "sections": '\
                        '[{"description": "Section", "time": 123, "errors": []}]}]}'

    @patch("analyzer.descriptors.patch_number_descriptor.resolve_redirect")
    def test_dump_analysis_text(self, redirector):
        """Test the text dumps of analysis"""
        redirector.return_value = self.EXAMPLE_GERRIT_URL
        descriptor = DescriptorFactory.build_descriptor("12345", "mediawiki/extensions/Test")
        analysis = Analysis(descriptor)
        analysis.add_job(456, "Job Name")
        analysis.add_section("Section", 123)
        self.assertEqual(self.EXAMPLE_TEXT_DUMP, analysis.dump(Analysis.FORMAT_TEXT))

    @patch("analyzer.descriptors.patch_number_descriptor.resolve_redirect")
    def test_dump_analysis_json(self, redirector):
        """Test the text dumps of analysis"""
        redirector.return_value = self.EXAMPLE_GERRIT_URL
        descriptor = DescriptorFactory.build_descriptor("12345", "mediawiki/extensions/Test")
        analysis = Analysis(descriptor)
        analysis.add_job(456, "Job Name")
        analysis.add_section("Section", 123)
        self.assertEqual(self.EXAMPLE_JSON_DUMP, analysis.dump(Analysis.FORMAT_JSON))
