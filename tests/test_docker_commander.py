"""It's a file of tests"""

import os
import re
import shlex
from unittest import TestCase

from analyzer.docker_commander import DockerCommander
from run_local import Config

class TestDockerCommander(TestCase):
    """Test cases for Docker commander"""

    EXAMPLE_COMMAND = ('docker run --entrypoint=quibble-with-supervisord '
                       '--tmpfs /workspace/db:size=320M --volume '
                       '/srv/jenkins/workspace/wmf-quibble-selenium-php74/src'
                       ':/workspace/src --volume /srv/jenkins/workspace/wmf-'
                       'quibble-selenium-php74/cache:/cache --volume '
                       '/srv/jenkins/workspace/wmf-quibble-selenium-php74/log:'
                       '/workspace/log --volume /srv/git:/srv/git:ro '
                       '--security-opt seccomp=unconfined --init --rm --label '
                       'jenkins.job=wmf-quibble-selenium-php74 --label '
                       'jenkins.build=8945 --env-file /dev/fd/63 '
                       'docker-registry.wikimedia.org/releng/quibble-buster-'
                       'php74:1.7.0-s2 --reporting-url='
                       'https://earlywarningbot.toolforge.org --packages-source'
                       ' vendor --db mysql --db-dir /workspace/db '
                       '--git-parallel=8 --reporting-url='
                       'https://earlywarningbot.toolforge.org --run selenium')

    EXAMPLE_LOCAL_COMMAND = ('docker run --entrypoint=quibble-with-supervisord '
                             '--tmpfs /workspace/db:size=320M '
                             '--security-opt seccomp=unconfined '
                             '--env-file=env_file '
                             '--volume PWD/src:/workspace/src '
                             '--volume PWD/cache:/cache --volume PWD/log:'
                             '/workspace/log --volume PWD/git:/srv/git:ro '
                             '--init --rm docker-registry.wikimedia.org/releng/'
                             'quibble-buster-php74:1.7.0-s2 --git-cache /srv/git '
                             '--git-parallel=8 '
                             '--packages-source vendor --db mysql --db-dir '
                             '/workspace/db --run selenium')

    EXAMPLE_QUIBBLE_COMMAND = (
                        'docker run --entrypoint=bash --workdir=/src '
                        '--tmpfs /workspace/db:size=320M '
                        '--volume /srv/jenkins/workspace/integration-quibble-'
                        'fullrun-extensions-phpunit/src:/src '
                        '--volume /srv/jenkins/workspace/integration-quibble-'
                        'fullrun-extensions-phpunit/cache:/cache '
                        '--volume /srv/jenkins/workspace/integration-quibble-'
                        'fullrun-extensions-phpunit/log:/workspace/log '
                        '--volume /srv/git:/srv/git:ro '
                        '--security-opt seccomp=unconfined --init --rm '
                        '--label jenkins.job=integration-quibble-fullrun-'
                        'extensions-phpunit --label jenkins.build=608 '
                        '--env-file /dev/fd/63 '
                        'docker-registry.wikimedia.org/releng/quibble-'
                        'buster-php74:1.10.0 '
                        '-c \'/usr/bin/supervisord -c '
                        '/etc/supervisor/supervisord.conf && '
                        '/src/utils/ci-fullrun-extensions.sh --color '
                        '--db-dir /workspace/db --run phpunit '
                        '--skip selenium,npm-test,qunit,api-testing '
                        '--web-backend=external --web-url=http://127.0.0.1:9413\''
                        )

    EXAMPLE_QUIBBLE_LOCAL_COMMAND = ('docker run '
                             '--entrypoint=bash '
                             '--tmpfs /workspace/db:size=320M '
                             '--security-opt seccomp=unconfined '
                             '--env-file=env_file '
                             '--volume PWD/src:/src '
                             '--volume PWD/cache:/cache --volume PWD/log:'
                             '/workspace/log --volume PWD/git:/srv/git:ro '
                             '--init --rm docker-registry.wikimedia.org/releng/'
                             'quibble-buster-php74:1.10.0 '
                             '-c \'/usr/bin/supervisord -c '
                             '/etc/supervisor/supervisord.conf && '
                             '/src/utils/ci-fullrun-extensions.sh --color '
                             '--db-dir /workspace/db --run phpunit '
                             '--skip selenium,npm-test,qunit,api-testing '
                             '--web-backend=external --web-url=http://127.0.0.1:9413\''
                             )

    EXAMPLE_LOCAL_COMMAND_WITH_CHANGE = ('docker run --entrypoint=quibble-with-supervisord '
                             '--tmpfs /workspace/db:size=320M '
                             '--security-opt seccomp=unconfined '
                             '--env-file=env_file '
                             '--volume PWD/src:/workspace/src '
                             '--volume PWD/cache:/cache --volume PWD/log:'
                             '/workspace/log --volume PWD/git:/srv/git:ro '
                             '--init --rm docker-registry.wikimedia.org/releng/'
                             'quibble-buster-php74:1.7.0-s2 --git-cache /srv/git '
                             '--change=1077358 '
                             '--git-parallel=8 '
                             '--packages-source vendor --db mysql --db-dir '
                             '/workspace/db --run selenium')


    def test_parse_command(self):
        """Test the parse command"""
        command = DockerCommander(self.EXAMPLE_COMMAND, Config("https://dummy", "."))
        self.assertEqual(
            shlex.split(re.sub("PWD", os.getcwd(), self.EXAMPLE_LOCAL_COMMAND)),
            command.generate_local_command("env_file", {})
        )

    def test_parse_quibble_command(self):
        """Test the parse command"""
        command = DockerCommander(self.EXAMPLE_QUIBBLE_COMMAND, Config("https://dummy", "."))
        self.assertEqual(
            shlex.split(re.sub("PWD", os.getcwd(), self.EXAMPLE_QUIBBLE_LOCAL_COMMAND)),
            command.generate_local_command("env_file", {})
        )

    def test_change_argument_is_processed(self):
        """Test that the ZUUL_CHANGE_IDS argument is included and passed to quibble"""
        command = DockerCommander(self.EXAMPLE_COMMAND, Config("https://dummy", "."))
        # Quibble's 'Change' argument only supports the latest patchset version
        env = { "ZUUL_CHANGE_IDS": "1077358,2" }
        self.assertEqual(
            shlex.split(re.sub("PWD", os.getcwd(), self.EXAMPLE_LOCAL_COMMAND_WITH_CHANGE)),
            command.generate_local_command("env_file", env)
        )
