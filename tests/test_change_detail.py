"""Tests of the ChangeDetail class"""
import os
from unittest import TestCase

from analyzer.change_detail import ChangeDetail

class TestChangeDetail(TestCase):
    """Tests of the ChangeDetail class"""

    def test_load_change_detail_by_change_id(self):
        """Test that we can parse a fixture from a change-id detail URL"""
        with open( os.path.join(
                os.path.dirname( __file__ ),
                "fixtures",
                "detail_for_change_id_I52227cbca1382f350ca734c8c935a5c9aa39d0d2"
             ), "rb" ) as f:
            detail = ChangeDetail(f.read())
            self.assertEqual("I52227cbca1382f350ca734c8c935a5c9aa39d0d2", detail.get_change_id())
            urls = detail.get_ci_urls()
            self.assertEqual(10, len(urls))
            self.assertEqual(
                    "https://integration.wikimedia.org/ci/job/" \
                    "quibble-vendor-mysql-php74-noselenium-docker/164023/console", urls[0])

    def test_load_change_detail_by_patch_number(self):
        """Test that we can parse a fixture from a patch number detail URL"""
        with open(
             os.path.join(
                os.path.dirname( __file__ ),
                "fixtures",
                "detail_for_patch_number_1013266" 
            ), "rb" ) as f:
            detail = ChangeDetail(f.read())
            self.assertEqual("I52227cbca1382f350ca734c8c935a5c9aa39d0d2", detail.get_change_id())
            urls = detail.get_ci_urls()
            self.assertEqual(10, len(urls))
            self.assertEqual(
                    "https://integration.wikimedia.org/ci/job/" \
                    "quibble-vendor-mysql-php74-noselenium-docker/164023/console", urls[0])
