"""Tests of the Analyzer module"""
import pathlib
import os
from unittest import TestCase
from unittest.mock import patch

from analyzer.parser import analyze_stream
from analyzer.descriptors.descriptor_factory import DescriptorFactory
from analyzer.analysis import Analysis

class TestAnalyzer(TestCase):
    """unittest class for the test cases for the Analyzer module"""

    def path_for_fixture(self, fixture_name):
        """Returns the path to a fixture file"""
        return os.path.join(pathlib.Path(__file__).parent.absolute(), "fixtures", fixture_name)

    def get_fixture_content(self, fixture_name):
        """Load the content of a fixture into a buffer"""
        with open(self.path_for_fixture(fixture_name), encoding='utf-8') as f:
            return f.read()

    @patch("analyzer.descriptors.patch_number_descriptor.resolve_redirect")
    def test_analyze_stream(self, redirector):
        """Test to see if we can analyze a result stream"""
        redirector.return_value = "https://gerrit.wikimedia.org/r/c/repo/+/123/"
        analysis = Analysis(DescriptorFactory.build_descriptor("123", "repo"))
        analysis.add_job(456, "Start of job")
        with open(self.path_for_fixture(
            "quibble-vendor-mysql-php74-noselenium-docker-164023-consoleFull"), "rb") as stream:
            analyze_stream(stream, analysis)
        self.assertEqual(analysis.dump_text(),
            self.get_fixture_content('quibble-vendor-mysql-php74-noselenium-docker-164023-parsed'))

    def test_analyze_local_stream(self):
        """Test to see if we can analyze a log from a local analysis"""
        analysis = Analysis(DescriptorFactory.build_descriptor(
            self.path_for_fixture("local-run-20240402174035.txt"), "repo"))
        analysis.add_job(456, "Start of job")
        with open(self.path_for_fixture("local-run-20240402174035.txt"),
                  encoding='utf-8') as stream:
            analyze_stream(stream, analysis)
        self.assertEqual(analysis.dump_text(),
            self.get_fixture_content('local-run-20240402174035-parsed'))
