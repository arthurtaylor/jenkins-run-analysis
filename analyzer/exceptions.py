"""Module containing the Exception classes for this project"""

class InvalidCiJobUrlException(Exception):
    """Exception thrown when an invalid CI URL is supplied"""

class InvalidGerritLinkException(Exception):
    """Exception thrown when an invalid Gerrit URL is supplied"""

class InvalidConfigException(Exception):
    """Exception thrown when the generated configuration is invalid"""

class MethodMissingException(Exception):
    """Exception thrown when the implementation is missing a method"""

class UnrecognisedChangeDescriptor(Exception):
    """Exception thrown if we are unable to process the provided
    string as a ChangeDescriptor"""

class InvalidLogOutputException(Exception):
    """Exception thrown when the log output is in an odd format"""

class MultipleChangesFoundException(Exception):
    """Exception thrown when our Patch specification returns multiple changes"""

    def __init__(self, change_path):
        """Create new MultipleChangesFoundException"""
        self.change_id = change_path

    def get_change_id(self):
        """Return the change_id"""
        return self.change_id

class ChangeDetailNotFoundException(Exception):
    """Exception thrown when our Detail API request returns Not Found"""

    def __init__(self, change_path):
        """Create a new ChangeDetailNotFoundException"""
        self.repo, self.patch_id = change_path.strip().split('~')

    def get_repo(self):
        """Return the name of the repo"""
        return self.repo

    def get_patch_id(self):
        """Return the patch ID"""
        return self.patch_id

class InvalidAnalysisFormatException(Exception):
    """Exception thrown when an invalid format for analysis output
    is requested"""

    def __init__(self, format_value):
        self.format = format_value
