"""Process docker invocations - manipulate arguments and launch docker"""

import argparse
from argparse import ArgumentParser
import os
import re
import shlex
import stat
import subprocess
import tempfile
from typing import List, Dict, Optional
from functools import reduce

from analyzer.config import Config

class DockerCommandException(Exception):
    """Exception related to docker commands"""

class SoftExitParser(ArgumentParser):
    """An Argument Parser that doesn't call sys.exit on failure"""

    def error(self, message):
        """In the event of an error, raise an exception"""
        raise DockerCommandException(message)

# pylint: disable=too-many-instance-attributes
class DockerCommander:
    """Manage docker invocations"""

    def __init__(self, command: str, options: Config) -> None:
        """Create a new docker commander from the supplied remote command string"""
        self.remote_command = command
        self.launch_bash = options.should_launch_bash()
        self.quibble_folder = options.get_quibble_folder()
        self.docker_image = options.get_docker_image()
        self.quibble_dev = options.get_quibble_dev()
        self.change = options.get_change()
        self.env_overrides = options.get_env_overrides()
        self.parallel_npm_install = options.should_parallel_npm_install()
        print(f"Got command '{command}'")
        if not self.remote_command.startswith("docker run"):
            raise DockerCommandException("Invalid docker command", command)

    def get_volume_args(self, parsed) -> List[str]:
        """Process the 'volume' arguments of the docker invocation, replacing remote
        paths with local ones"""
        def rebase(path):
            parts = path.split(":", 1)
            return os.path.join(self.quibble_folder, parts[0].split("/")[-1]) + f":{parts[1]}"
        print("Volume", parsed.volume)
        volumes = reduce(lambda acc, item: acc + item,
                      [ [ "--volume", rebase(volume[0]) ] for volume in parsed.volume ], [])
        if self.quibble_dev is not None:
            volumes += [
                "--volume",
                f"{self.quibble_dev}:/quibble",
                "--volume",
                f"{self.dev_scripts_path()}:/devscripts"
            ]
        print("Mapped to", volumes)
        return volumes

    def dev_scripts_path(self) -> str:
        """Return the path to the dev scripts folder"""
        return os.path.join(os.path.dirname(__file__), "..", "docker-scripts")

    def get_change_param(self, env: Dict) -> Optional[str]:
        """Extract the ChangeID to run Quibble with. If supplied on the command-line,
        use the CLI argument. Otherwise use the argument parsed from the environment"""
        res = None
        if self.change is not None:
            res = self.change
        elif "ZUUL_CHANGE_IDS" in env:
            res = env["ZUUL_CHANGE_IDS"]
        if res is None:
            return None
        # We can only process one change at a time - get the end of the chain
        change = res.split(" ")[-1]
        # We can only process the base revision
        base_change_id = change.split(",")[0]
        return base_change_id

    def process_quibble_args(self, args: List, env: Dict) -> List:
        """Process the quibble arguments of the docker invocation"""
        parsed = self.build_quibble_args_parser().parse_args(args)
        base = [ "--git-cache", "/srv/git" ]
        if self.get_change_param(env) is not None:
            base += [ f"--change={self.get_change_param(env)}" ]
        if parsed.git_parallel:
            base += [ f"--git-parallel={parsed.git_parallel}" ]
        if self.launch_bash:
            base += [ "-c", "bash" ]
            return base
        if parsed.packages_source:
            base += [ "--packages-source", parsed.packages_source ]
        if parsed.db:
            base += [ "--db", parsed.db ]
        if parsed.db_dir:
            base += [ "--db-dir", parsed.db_dir ]
        if parsed.run is not None:
            base += [ "--run", parsed.run ]
        if parsed.skip is not None:
            base += [ "--skip", parsed.skip ]
        if parsed.commands is not None:
            base += [ "--commands", parsed.commands ]
        if parsed.phpunit_testsuite is not None:
            base += [ f"--phpunit-testsuite={parsed.phpunit_testsuite}" ]
        return base

    def process_bash_args(self, args) -> List:
        """Process the bash arguments of the docker invocation"""
        parsed = self.build_bash_args_parser().parse_args(args)
        base = []
        if parsed.command:
            base += [ "-c", parsed.command ]
        return base

    def is_quibble_command(self) -> bool:
        """Returns true if the current docker command runs a quibble docker image"""
        parsed = self.get_parsed_arguments()
        return re.match(r".*docker-registry.wikimedia.org/releng/quibble.*",
                        parsed.image_name) is not None

    def get_parsed_arguments(self):
        """Uses `shlex` to parse a command string into arguments and processes
        the arguments with ArgumentParser"""
        args = shlex.split(self.remote_command)
        args = args[2:]
        return self.build_docker_args_parser().parse_args(args)

    def generate_local_command(self, env_file_name: str, env: Dict) -> List:
        """Generate a version of the remote command suitable for executing
        on the local machine"""
        parsed = self.get_parsed_arguments()
        base = [ "docker", "run" ]
        if self.launch_bash:
            base += [ "-it", "-p9413:9413" ]
        entrypoint = parsed.entrypoint
        if self.quibble_dev is not None:
            entrypoint = "/devscripts/quibble-with-supervisord"
        base += [ f"--entrypoint={entrypoint}",
                "--tmpfs", parsed.tmpfs,
                "--security-opt", parsed.security_opt,
                f"--env-file={env_file_name}" ]
        base += self.get_volume_args(parsed)
        if parsed.init:
            base += [ "--init" ]
        if parsed.rm:
            base += [ "--rm" ]
        # Only add our quibble arguments if we're launching quibble
        rest = []
        if "quibble" in entrypoint:
            rest = self.process_quibble_args(parsed.non_docker_args, env)
        elif "bash" == entrypoint and not self.launch_bash:
            rest = self.process_bash_args(parsed.non_docker_args)
        image_name = parsed.image_name
        if self.docker_image is not None:
            image_name = self.docker_image
        return base + [ image_name ] + rest

    def group_writable(self, folder):
        """Check if the supplied path can be written by all users"""
        s = os.stat(folder)
        mode = s[stat.ST_MODE]
        return mode & (stat.S_IWOTH | stat.S_IXOTH)

    def ensure_folders(self):
        """Create the folders necessary for Quibble if they don't exist"""
        if not os.path.isdir(self.quibble_folder):
            print(f"Target folder for execution ({self.quibble_folder}) "
                  "not found... creating")
            os.makedirs(self.quibble_folder, mode=0o777)
        for folder in [ 'cache', 'log', 'git', 'src' ]:
            target = os.path.join(self.quibble_folder, folder)
            if not os.path.isdir(os.path.join(self.quibble_folder, folder)):
                print(f"Creating volume mount folder {target}")
                os.makedirs(target, mode=0o777)
                os.chmod(target, 0o777)
            if not self.group_writable(target):
                print(f"Warning: folder {target} exists but is not world writable")

    def launch(self, env):
        """Launch a docker container to do what the remote container was doing"""
        self.ensure_folders()
        with tempfile.NamedTemporaryFile(mode='w', encoding='utf-8') as env_file:
            env_file.seek(0)
            for key, value in env.items():
                env_file.write(f"{key}={value}\n")
            env_file.flush()
            local_command = self.generate_local_command(env_file.name, env)
            print("Running local command", local_command)
            subprocess.run(local_command, check=True)

    def build_quibble_args_parser(self) -> ArgumentParser:
        """Build the quibble parts of the local invocation"""
        parser = SoftExitParser(prog='quibble', exit_on_error=False)
        parser.add_argument('--reporting-url')
        parser.add_argument('--packages-source')
        parser.add_argument('--db')
        parser.add_argument('--db-dir')
        parser.add_argument('--git-parallel')
        parser.add_argument('--phpunit-testsuite')
        parser.add_argument('--memcached-server')
        parser.add_argument('--commands')
        parser.add_argument('--success-cache-key-data')
        parser.add_argument('--skip')
        parser.add_argument('--run')
        return parser

    def build_bash_args_parser(self) -> ArgumentParser:
        """Build the bash parts of the local invocation"""
        parser = SoftExitParser(prog='bash', exit_on_error=False)
        parser.add_argument('-c', '--command')
        return parser

    def build_docker_args_parser(self) -> ArgumentParser:
        """Build the docker parts of the local invocation"""
        parser = SoftExitParser(prog='docker-run', exit_on_error=False)
        parser.add_argument("--entrypoint")
        parser.add_argument("--tmpfs")
        parser.add_argument("--volume", nargs='*', action='append')
        parser.add_argument("--security-opt")
        parser.add_argument("--label")
        parser.add_argument("--env-file")
        parser.add_argument("--user")
        parser.add_argument("--workdir")
        parser.add_argument("--init", action='store_true')
        parser.add_argument("--rm", action='store_true')
        parser.add_argument("image_name")
        parser.add_argument("non_docker_args", nargs=argparse.REMAINDER)
        return parser
