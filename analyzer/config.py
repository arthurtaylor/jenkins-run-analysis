"""Config object for the run_local script"""
import os
from typing import Dict, Optional

# pylint: disable=too-many-instance-attributes
class Config:
    """Class do hold the configuration for the command-line runner"""

    # pylint: disable=too-many-arguments,too-many-positional-arguments
    def __init__(
        self,
        url,
        quibble_folder,
        launch_bash = False,
        docker_image = None,
        quibble_dev = None,
        change = None,
        parallel_npm_install = False,
        env_overrides = None
    ):
        """Initialise the options object"""
        self.url = url
        self.launch_bash = launch_bash
        self.quibble_folder = os.path.abspath(quibble_folder)
        self.docker_image = docker_image
        self.quibble_dev = quibble_dev
        self.change = change
        self.parallel_npm_install = parallel_npm_install
        if env_overrides:
            self.env_overrides = env_overrides
        else:
            self.env_overrides = {}

    def should_launch_bash(self):
        """True if bash should be launched"""
        return self.launch_bash

    def source_is_yaml(self) -> bool:
        """Returns true if the config source is a yaml file"""
        return self.url.startswith('file://') and self.url.endswith('.yaml')

    def get_file_path(self) -> Optional[str]:
        """Returns the file path of a local config"""
        if not self.url.startswith('file://'):
            return None
        return self.url[7:]

    def get_url(self):
        """The URL of the run to reproduce"""
        return self.url

    def get_quibble_folder(self):
        """The folder within which to exexcute quibble"""
        return self.quibble_folder

    def get_docker_image(self):
        """The docker image to use (if specified)"""
        return self.docker_image

    def get_quibble_dev(self):
        """The folder for the Quibble source code"""
        return self.quibble_dev

    def get_change(self):
        """The selected ChangeID"""
        return self.change

    def get_env_overrides(self) -> Dict:
        """Return a dictionary of environment overrides"""
        return self.env_overrides

    def should_parallel_npm_install(self):
        """Return true if the NPM installs should be done in parallel"""
        return self.parallel_npm_install
