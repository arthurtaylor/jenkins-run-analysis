"""Takes a change descriptor and extracts a list of URLs for connected CI jobs"""
import logging

import requests

from analyzer.change_detail import ChangeDetail

logger = logging.getLogger()

class ChangeDescriptionResolver: # pylint: disable=too-few-public-methods
    """Simple class with static method to resolve CI jobs for a change descriptor"""

    @classmethod
    def resolve_ci_urls_for_change(cls, descriptor):
        """Turn a change descriptor into a list of CI URLs"""
        logger.debug("Fetching from %s", descriptor.gerrit_detail_url())
        return ChangeDetail(
            requests.get( descriptor.gerrit_detail_url(), timeout=30 ).content ).get_ci_urls()
