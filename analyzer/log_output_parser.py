"""Module with functions for parsing the lines of the CI log output"""
import re
from time import strptime, mktime
from html import unescape

def start_error_section(line):
    """Return true if the line marks the start of an error section"""
    return re.match(r".*(There was 1 failure:|There were \d+ failures).*", line) is not None

def unescape_urls(text):
    """Unescape HTML-escaped links in log output"""
    return re.sub(r"<a href='[^']*'>([^<]+)</a>", r"\1", text)

def unescape_ampersand(text):
    """Unescape &amp; entities in log output"""
    return text.replace("&amp;", "&")

def extract_docker_command(line):
    """Return the command used to run quibble"""
    run_match = re.match(r".*exec (docker run --entrypoint=.*)$", line)
    if run_match is None:
        return None
    return unescape_ampersand(unescape_urls(run_match.group(1)))

def extract_error_text_plain(line):
    """Extract the error text from a plain text line"""
    error_match = re.match(r"^\d{2}:\d{2}:\d{2} (.*)$", line)
    if error_match is None:
        return ""
    return error_match.group(1)

def extract_error_text_html(line):
    """Extract the error text from an HTML line"""
    error_match = re.match(r".*<b>\d{2}:\d{2}:\d{2}<\/b> </span>(.*)", line)
    if error_match is None:
        return ""
    return error_match.group(1)

def extract_error_text(line):
    """Extract the content of the error output from the log line"""
    if line.startswith('<span'):
        return extract_error_text_html(line)
    return extract_error_text_plain(line)

def extract_timestamp_html(line):
    """Extract a time value from the timestamp in the HTML log"""
    stamp = re.match(r".*<b>(\d{2}:\d{2}:\d{2})<\/b>.*", line)
    if stamp is None:
        return None
    return stamp.group(1)

def extract_timestamp_plain(line):
    """Extract a time value from the timestamp in the plain log"""
    stamp = re.match(r"^(\d{2}:\d{2}:\d{2}) .*", line)
    if stamp is None:
        return None
    return stamp.group(1)

def extract_timestamp(line):
    """Extract a time value from the timestamp in the log"""
    text = extract_timestamp_html(line)
    if text is None:
        text = extract_timestamp_plain(line)
    if text is None:
        return None
    return strptime(text, "%H:%M:%S")

def extract_header_html(line):
    """Extract a section header from the HTML log output"""
    header = re.match(
        r".*<div class=\"collapseHeader\">(.*)<div class=\"collapseAction\">.*", line)
    if header is None:
        return None
    return unescape(header.group(1))

def extract_header_plain(line):
    """Extract a section header from the plain log output"""
    header = re.match(r"^.*:>>> Start: (.*)$", line)
    if header is None:
        return None
    string = header.group(1)
    if string.endswith(":"):
        return string[:-1]
    return string

def extract_header(line):
    """Extract a section header from the log output"""
    header = extract_header_html(line)
    if header is None:
        header = extract_header_plain(line)
    if header is None:
        return None
    return header

def time_diff(start, end):
    """Return a diff between two times"""
    return int(mktime(end) - mktime(start))
