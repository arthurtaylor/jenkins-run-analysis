"""Extract the ENV variables for a build be supplying the gerrit URL for the
   build parameters page"""

import requests

from bs4 import BeautifulSoup

class ParameterManager:
    """Class for fetching CI environment variables given a job run URL"""

    SKIP = [
        'ZUUL_URL',
        'ZUUL_REF',
        'BASE_LOG_PATH',
        'LOG_PATH'
    ]

    def extract_variables(self, form_element):
        """Extract the ENV variable setting from a jenkins-form-item element
        as a key value pair"""
        label = form_element.find('div', class_='jenkins-form-label').text
        setting = form_element.find('textarea').text
        if label in self.SKIP or str(setting) == "":
            return None
        return [str(label), str(setting)]

    def get_content(self, url):
        """Fetch the contents of the supplied URL as a soup"""
        return BeautifulSoup(requests.get(url, timeout=30).content, features="html.parser")

    def parse_to_dictionary(self, url):
        """Return the environment variables as a dictionary"""
        variables = {}
        for element in self.get_content(url).find_all('div', class_="jenkins-form-item"):
            pair = self.extract_variables(element)
            if pair is not None:
                variables[pair[0]] = pair[1]
        return variables

    def parse_to_env_file(self, url):
        """Return the environment variables as KEY=value lines"""
        lines = []
        for key, value in self.parse_to_dictionary(url).items():
            lines.append(f"{key}={value}")
        return lines
