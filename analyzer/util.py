"""Module for utility classes and functions"""
import requests

class JSONSerializable: # pylint: disable=too-few-public-methods
    """Interface for serializable objects"""

    def to_json(self):
        """Convert the object to json"""
        return self.__dict__

def resolve_redirect(url):
    """Return the location of a 301 redirect from a provided URL"""
    redirect = requests.get(url, timeout = 30)
    return redirect.url

def smart_decode(str_input: str|bytes) -> str:
    """Turns a string or byte array into a unicode string"""
    if isinstance(str_input, memoryview):
        return str_input.tobytes().decode('utf-8')
    if isinstance(str_input, (bytes, bytearray)):
        return str_input.decode('utf-8')
    return str_input
