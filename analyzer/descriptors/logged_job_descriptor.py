"""Module for the LoggedJobDescriptor class"""
import abc
from typing import Optional

from analyzer.descriptors.job_descriptor import JobDescriptor
from analyzer.exceptions import MethodMissingException
from analyzer.util import smart_decode
from analyzer.log_output_parser import extract_docker_command
from analyzer.docker_commander import DockerCommander
from analyzer.config import Config

class LoggedJobDescriptor(JobDescriptor):
    """Job descriptor for a job where a full console log is available.
    Can be used by the job analyzer.
    """

    def get_quibble_docker_command(self, config: Config) -> Optional[DockerCommander]:
        """Return the command used to launch the docker container
        that ran Quibble during the CI run"""
        for line in self.get_log_stream().readlines():
            line_utf8 = smart_decode(line)
            docker_command = extract_docker_command(line_utf8)
            if docker_command is not None:
                commander = DockerCommander( docker_command, config )
                if commander.is_quibble_command():
                    return commander
        return None

    @abc.abstractmethod
    def get_log_stream(self):
        """Returns the stream of log data for this job"""
        raise MethodMissingException(
            f"get_log_stream must be implemented in subclass {self.__class__.__name__}")
