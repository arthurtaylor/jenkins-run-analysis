"""Change Descriptor based on patch number"""
import re

from analyzer.descriptors.gerrit_link_descriptor import GerritLinkDescriptor
from analyzer.util import resolve_redirect

class GerritShortLinkDescriptor(GerritLinkDescriptor):
    """Class representing a Change Descriptor"""

    def __init__(self, url: str, repo: str):
        """Create a new patch descriptor based on a patch number."""
        target = resolve_redirect(url)
        super(GerritShortLinkDescriptor, self).__init__(target, repo) # pylint: disable=super-with-arguments

    @classmethod
    def handles(cls, path: str) -> bool:
        """Returns True if this ChangeDescriptor can process the provided identifier"""
        return re.match( r"https://gerrit.wikimedia.org/r/\d+", path ) is not None
    