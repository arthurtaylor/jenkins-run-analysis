"""Module for the DescriptorFactory class"""

from analyzer.descriptors.descriptor import Descriptor
from analyzer.descriptors.url_descriptor import UrlDescriptor
from analyzer.descriptors.file_descriptor import FileDescriptor
from analyzer.descriptors.patch_number_descriptor import PatchNumberDescriptor
from analyzer.descriptors.change_id_descriptor import ChangeIdDescriptor
from analyzer.descriptors.gerrit_link_descriptor import GerritLinkDescriptor
from analyzer.descriptors.gerrit_short_link_descriptor import GerritShortLinkDescriptor
from analyzer.exceptions import UnrecognisedChangeDescriptor

class DescriptorFactory: # pylint: disable=too-few-public-methods
    """Factory class for building ChangeDescriptors"""

    @classmethod
    def build_descriptor(
        cls,
        string: str,
        repo: str
    ) -> Descriptor:
        """Takes a string and returns an appropriate ChangeDescriptor"""
        for descriptor_type in [
                FileDescriptor,
                UrlDescriptor,
                PatchNumberDescriptor,
                ChangeIdDescriptor,
                GerritLinkDescriptor,
                GerritShortLinkDescriptor
            ]:
            if descriptor_type.handles( string ):
                return descriptor_type( string, repo = repo )
        raise UnrecognisedChangeDescriptor(f"Unrecognised object reference: {string}")
    