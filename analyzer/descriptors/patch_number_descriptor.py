"""Change Descriptor based on patch number"""
import re

from analyzer.descriptors.gerrit_link_descriptor import GerritLinkDescriptor
from analyzer.util import resolve_redirect

class PatchNumberDescriptor(GerritLinkDescriptor):
    """Class representing a Change Descriptor"""

    def __init__(self, patch_number_string: str, repo: str):
        """Create a new patch descriptor based on a patch number."""
        url = resolve_redirect(f"https://gerrit.wikimedia.org/r/{patch_number_string}")
        super(PatchNumberDescriptor, self).__init__(url, repo) # pylint: disable=super-with-arguments

    @classmethod
    def handles(cls, path: str) -> bool:
        """Returns True if this ChangeDescriptor can process the provided identifier"""
        return re.match( r"^\d+$", path ) is not None
    