"""Module containing the UrlDescriptor class"""
import os

from analyzer.descriptors.descriptor import Descriptor
from analyzer.descriptors.local_job_descriptor import LocalJobDescriptor

class FileDescriptor(Descriptor):
    """Class wrapping a Filename as a ChangeDescriptor"""

    def __init__(self, filename: str, repo: str):
        """Create a new UrlDescriptor on the basis of a URL to a CI job run"""
        self.filename = filename
        self.repo = repo
        self.job_descriptor = LocalJobDescriptor("Local", "Local", filename)

    def get_runs(self):
        """Return a list of runs for this ChangeDescriptor (in this case one job)"""
        return [ self.job_descriptor ]

    def pretty_heading(self) -> str:
        """Returns a pretty description of the job"""
        return f"{self.repo} - {self.job_descriptor}"

    @classmethod
    def handles(cls, path: str) -> bool:
        """Returns True if we are able to parse this string as an existing file on the filesystem"""
        return os.path.exists(path)
