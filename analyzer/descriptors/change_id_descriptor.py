"""Change Descriptor based on Gerrit ChangeId number"""
import re
from typing import List

from analyzer.descriptors.descriptor import Descriptor
from analyzer.descriptors.ci_job_descriptor import CiJobDescriptor
from analyzer.change_description_resolver import ChangeDescriptionResolver

class ChangeIdDescriptor(Descriptor):
    """Class representing a Change Descriptor"""

    def __init__(self, change_id: str, repo: str):
        """Create a new patch descriptor based on a gerrit ChangeId"""
        self.change_id = change_id
        self.repo = repo

    def get_runs(self) -> List[CiJobDescriptor]:
        """Return a list of CiJobDescriptor objects corresponding to the CI jobs
        associated with this change"""
        return [ CiJobDescriptor.from_url(url)
                for url in ChangeDescriptionResolver.resolve_ci_urls_for_change(self) ]

    def gerrit_detail_url(self) -> str:
        """Return the gerrit detail URL for this ChangeDescriptor"""
        return f"https://gerrit.wikimedia.org/r/changes/{self.change_id}/detail"

    def pretty_heading(self) -> str:
        return f"Gerrit Change ID {self.change_id}"

    def __str__(self):
        """Return a string description"""
        return f"ChangeId({self.change_id})"

    @classmethod
    def handles(cls, path: str) -> bool:
        """Returns True if this ChangeDescriptor can process the provided identifier"""
        return re.match( r"^I[a-f0-9]{40}$", path ) is not None
    