"""Module for the CiJobDescriptor class"""
from analyzer.descriptors.logged_job_descriptor import LoggedJobDescriptor

class LocalJobDescriptor(LoggedJobDescriptor):
    """Class representing a link to a CI job execution"""

    def __init__(self, job_name, run_id, filename):
        """Create a new CiJobDescriptor"""
        super().__init__(job_name, run_id)
        self.filename = filename

    def get_log_stream(self):
        """Returns a stream corresponding to the log output for the job"""
        return open(self.filename, encoding='utf-8') # pylint: disable=consider-using-with
