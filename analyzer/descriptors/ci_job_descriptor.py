"""Module for the CiJobDescriptor class"""
import re

from functools import partial
import requests

from analyzer.descriptors.logged_job_descriptor import LoggedJobDescriptor
from analyzer.exceptions import InvalidCiJobUrlException

class CiJobDescriptor(LoggedJobDescriptor):
    """Class representing a link to a CI job execution"""

    URL_BASE = "https://integration.wikimedia.org/ci/job"
    PATTERN = re.compile( rf"{URL_BASE}/(.*)/(\d+)/.*" )

    def __init__(self, job_name, run_id, url):
        """Create a new CiJobDescriptor"""
        super().__init__(job_name, run_id)
        self.url = url

    def get_console_url(self):
        """Get the main job output URL for this Descriptor"""
        return f"{self.URL_BASE}/{self.job_name}/{self.run_id}/console"

    def get_parameters_url(self):
        """Get the parameter URL for this Descriptor"""
        return f"{self.URL_BASE}/{self.job_name}/{self.run_id}/parameters/"

    def get_log_stream(self):
        """Takes a URL pointing to a gerrit console log, and analyses the data
        streamed from the URL"""
        log_output_url = f"{self.URL_BASE}/{self.job_name}/{self.run_id}/consoleFull"
        response = requests.get(log_output_url, stream = True, timeout = 30)
        response.raw.read = partial(response.raw.read, decode_content=True)
        return response.raw

    @classmethod
    def from_url(cls, url: str):
        """Build a new CiJobDescriptor from a CI URL"""
        match = cls.PATTERN.match( url )
        if match is None:
            raise InvalidCiJobUrlException(f"Invalid URL for CI job: {url}")
        return CiJobDescriptor(match.group(1), match.group(2), url)
