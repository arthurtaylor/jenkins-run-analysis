"""Module for the JobDescriptor class"""
import abc
from typing import Optional

from analyzer.exceptions import MethodMissingException
from analyzer.util import JSONSerializable
from analyzer.config import Config
from analyzer.docker_commander import DockerCommander

class JobDescriptor(JSONSerializable):
    """Class representing a link to a job execution"""

    def __init__(self, job_name, run_id):
        """Create a new JobDescriptor"""
        self.job_name = job_name
        self.run_id = run_id

    def get_job_name(self):
        """Get the job name"""
        return self.job_name

    def get_run_id(self):
        """Get the run ID"""
        return self.run_id

    @abc.abstractmethod
    def get_quibble_docker_command(self, config: Config) -> Optional[DockerCommander]:
        """Returns the docker command necessary to reproduce the run"""
        raise MethodMissingException("get_quibble_docker_command must be implemented in subclass")

    def __str__(self):
        """Return the string representation of the Job Descriptor"""
        return f"{self.job_name} ({self.run_id})"
