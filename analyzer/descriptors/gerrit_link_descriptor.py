"""Change Descriptor based on gerrit link"""
import re
from urllib.parse import quote_plus
from typing import List

from analyzer.descriptors.descriptor import Descriptor
from analyzer.descriptors.ci_job_descriptor import CiJobDescriptor
from analyzer.change_description_resolver import ChangeDescriptionResolver
from analyzer.exceptions import InvalidGerritLinkException

class GerritLinkDescriptor(Descriptor):
    """Class representing a Change Descriptor"""

    URL_FORMAT = re.compile(r"https://gerrit.wikimedia.org/r/c/(.*)/\+/(\d+)/?")

    def __init__(self, gerrit_link: str, repo: str): # pylint: disable=unused-argument
        """Create a new patch descriptor based on a patch number. If no project / repo
        is supplied, `mediawiki/extensions/Wikibase` will be assumed"""
        self.url = gerrit_link
        match = self.URL_FORMAT.match(self.url)
        if match is None:
            raise InvalidGerritLinkException(gerrit_link)
        self.repo = match.group(1)
        self.patch_number = match.group(2)

    def get_runs(self) -> List[CiJobDescriptor]:
        """Return a list of CiJobDescriptor objects corresponding to the CI jobs
        associated with this change"""
        return [ CiJobDescriptor.from_url(url)
                for url in ChangeDescriptionResolver.resolve_ci_urls_for_change(self) ]

    def gerrit_detail_url(self) -> str:
        """Return the gerrit detail URL for this ChangeDescriptor"""
        return "https://gerrit.wikimedia.org/r/changes/" \
            f"{quote_plus(self.repo)}~{self.patch_number}/detail"

    def __str__(self) -> str:
        """Return a string description"""
        return f"PatchNumber({self.patch_number})"

    def pretty_heading(self) -> str:
        return f"{self.repo}~{self.patch_number}"

    @classmethod
    def handles(cls, path: str) -> bool:
        """Returns True if this ChangeDescriptor can process the provided identifier"""
        return cls.URL_FORMAT.match(path) is not None
    