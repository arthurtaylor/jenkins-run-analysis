"""Module containing the UrlDescriptor class"""
from analyzer.descriptors.descriptor import Descriptor
from analyzer.descriptors.ci_job_descriptor import CiJobDescriptor

class UrlDescriptor(Descriptor):
    """Class wrapping a CI Job URL as a ChangeDescriptor"""

    def __init__(self, url: str, repo: str):
        """Create a new UrlDescriptor on the basis of a URL to a CI job run"""
        self.url = url
        self.repo = repo
        self.ci_job_descriptor = CiJobDescriptor.from_url(url)

    def get_runs(self):
        """Return a list of runs for this ChangeDescriptor (in this case one job)"""
        return [ self.ci_job_descriptor ]

    def pretty_heading(self) -> str:
        """Returns a pretty description of the job"""
        return f"{self.repo} - {self.ci_job_descriptor}"

    @classmethod
    def handles(cls, path: str) -> bool:
        """Returns True if we are able to parse this string as a link to a CI job"""
        return CiJobDescriptor.PATTERN.match( path ) is not None
