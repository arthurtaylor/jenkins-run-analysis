"""Module for the YamlJobDescriptor class"""
from typing import Optional
import yaml

from analyzer.descriptors.job_descriptor import JobDescriptor
from analyzer.config import Config
from analyzer.docker_commander import DockerCommander

class YamlJobDescriptor(JobDescriptor):
    """Load the job description from a Yaml file"""

    def __init__(self, data):
        super().__init__("Yaml", "Yaml")
        self.env = data['env']
        self.dockercmd = data['dockercmd']

    def get_env_variables(self):
        """Return the dictionary of environment variables for the job"""
        return self.env

    def get_quibble_docker_command(self, config: Config) -> Optional[DockerCommander]:
        """Return the docker command for the job"""
        commander = DockerCommander( self.dockercmd, config )
        if commander.is_quibble_command():
            return commander
        return None

    @classmethod
    def from_yaml(cls, path: str):
        """Generate a YamlJobDescriptor from a local Yaml file"""
        with open(path, encoding='utf-8') as f:
            return YamlJobDescriptor(yaml.safe_load(f))
