"""Module containing the Descriptor abstract class"""
import abc
from typing import Sequence

from analyzer.descriptors.job_descriptor import JobDescriptor
from analyzer.exceptions import MethodMissingException
from analyzer.util import JSONSerializable

class Descriptor(JSONSerializable):
    """Abstract parent class of all Job Descriptor classes"""

    @abc.abstractmethod
    def get_runs(self) -> Sequence[JobDescriptor]:
        """Abstract method. Implement this in subclass to return a list of CI URLs"""
        raise MethodMissingException(
            f"get_runs must be implemented in subclass {self.__class__.__name__}")

    @classmethod
    def handles( cls, path: str ) -> bool: # pylint: disable=unused-argument
        """Returns a true if the concrete subclass can handle the supplied job descriptor string"""
        return False

    @abc.abstractmethod
    def pretty_heading(self) -> str:
        """Abstract method to pretty print the change descriptor as a string"""
        raise MethodMissingException(
            f"pretty_heading must be implemented in subclass {self.__class__.__name__}")
