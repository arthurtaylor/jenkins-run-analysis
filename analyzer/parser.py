"""Module for processing the raw Gerrit console log output from CI jobs"""
import re

from analyzer.descriptors.logged_job_descriptor import LoggedJobDescriptor
from analyzer.log_output_parser import \
    time_diff, extract_header, extract_timestamp, start_error_section, extract_error_text
from analyzer.analysis import Analysis
from analyzer.util import smart_decode

def analyze_stream(stream, analysis: Analysis) -> Analysis:
    """Takes a stream containing Gerrit CI log data and prints timings and section
    headings"""
    start_time = None
    last_time = None
    in_error = False
    error_output = []
    current_section = "Setup"
    for line in stream.readlines():
        line_utf8 = smart_decode(line)
        if re.match(r".*<title>Not Found \[Jenkins\]<\/title>.*", line_utf8):
            analysis.mark_job_empty()
            return analysis
        header = extract_header(line_utf8)
        timestamp = extract_timestamp(line_utf8)
        if start_error_section(line_utf8):
            in_error = True
            continue
        if in_error:
            error_output.append(extract_error_text(line_utf8))
        if header is not None and "split_group_" not in header:
            analysis.add_section(current_section, time_diff(start_time, last_time), error_output)
            current_section = header
            start_time = last_time
            in_error = False
            error_output = []

        if timestamp is not None:
            if start_time is None:
                start_time = timestamp
            last_time = timestamp

    if start_time is not None and last_time is not None:
        analysis.add_section(current_section, time_diff(start_time, last_time))
    return analysis

def analyze_run(run: LoggedJobDescriptor, analysis: Analysis):
    """Takes a Job Descriptor and analyses the log stream"""
    return analyze_stream(run.get_log_stream(), analysis)

def analyze(descriptor):
    """Takes a Change Descriptor, finds the associated CI runs, and analyses
    the log output of the CI jobs"""
    runs = descriptor.get_runs()
    if len( runs ) == 0:
        print(f"No runs found for descriptor {descriptor}")
        return None
    analysis = Analysis(descriptor)
    for run in runs:
        analysis.add_job(run.get_run_id(), run.get_job_name())
        analyze_run(run, analysis)
    return analysis
