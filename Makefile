default: test types lint

test:
	pytest

lint:
	pylint *.py analyzer tests

types:
	pyright

.PHONY: lint types test
