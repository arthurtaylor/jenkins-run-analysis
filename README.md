Jenkins Run Analysis
===

Small Python-based tool to extract information about the execution of Jenkins jobs.


## Installation

Use `pipenv` to install the dependencies and launch a shell with the required environment:

```
pipenv install
pipenv shell
```

## Usage

### Analyzing Data from CI Runs

The tool takes a Gerrit Patch ID, a Gerrit URL, a ChangeSet ID or a build output URL, and outputs timing for the CI jobs:

```
python analyze.py https://gerrit.wikimedia.org/r/1013266
python analyze.py 1013266
python analyze.py I52227cbca1382f350ca734c8c935a5c9aa39d0d2
python analyze.py https://integration.wikimedia.org/ci/job/quibble-vendor-mysql-php74-noselenium-docker/164023/console
python analyze.py https://gerrit.wikimedia.org/r/c/mediawiki/extensions/Wikibase/+/1019817
```

(or use `analyze.sh` from outside of the `pipenv shell` to launch the script in a `pipenv` context).

By default, the tool outputs a human-readable report, but supplying `--format=json` will output a JSON-format report.

Also included in this distribution is the `extract_env.py` script, which can be used to download the set of environment variables used in a build. This script takes the URL of the 'parameters' Jenkins page as a parameter:

```
 python extract_env.py https://integration.wikimedia.org/ci/job/quibble-vendor-mysql-php74-selenium/12346/parameters/
```

### Reproducing CI Runs locally

The script `run_local.py` allows you to reproduce a CI execution on your local
machine simply by supplying the URL. The script will extract the environment
parameters and docker command-line, and execute Quibble in a docker container
on your local machine.

Simply supply the URL of the target CI run that you want to reproduce:

```
python run_local.py https://integration.wikimedia.org/ci/job/mediawiki-quibble-apitests-vendor-php74/6051/console
```

This will create a `quibble` folder in the current working directory (by default)
which will contain the local volume mounts for the docker execution. Note that on
the first run, the execution will take longer as the source code for the whole of
Mediawiki and extensions will be downloaded. Subsequent runs will be faster. Also
note that if the first run times out with a download error, you can just re-run
the command to retry.

Supplying the `--bash` option will drop you into a shell instead of running the
target test suites. This can be useful for debugging runs.

You can configure the target folder for the volume mounts by setting `--target`

## Development

The tests can be run with `pytest`, the linter with `pylint` and the type checker with `pyright`. Alternatively, run `make` to execute all three (recommended before committing).
